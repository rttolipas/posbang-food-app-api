<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->integer('coupon_id')->default(0);
            $table->integer('tax_id')->default(0);
            $table->integer('status_id')->default(0);
            $table->decimal('total_amount', 7, 2)->default(0);
            $table->timestamps();
        });

        Schema::create('order_transaction', function (Blueprint $table) {
            $table->integer('order_id');
            $table->integer('transaction_id');
            $table->unique(['order_id', 'transaction_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('order_transaction');
    }
}
