<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->nullable();
            $table->decimal('price', 7, 2)->nullable();
            $table->mediumText('image')->nullable();
            $table->integer('category_id')->nullable();
            $table->timestamps();
        });

        Schema::create('meal_product', function (Blueprint $table) {
            $table->integer('meal_id');
            $table->integer('product_id');
            $table->unique(['meal_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meals');
        Schema::dropIfExists('meal_product');
    }
}
