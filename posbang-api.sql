-- -------------------------------------------------------------
-- TablePlus 3.5.3(314)
--
-- https://tableplus.com/
--
-- Database: posbang-api
-- Generation Time: 2020-06-01 20:47:50.8820
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `code` char(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` tinyint unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `meal_product`;
CREATE TABLE `meal_product` (
  `meal_id` int NOT NULL,
  `product_id` int NOT NULL,
  UNIQUE KEY `meal_product_meal_id_product_id_unique` (`meal_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `meals`;
CREATE TABLE `meals` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(7,2) DEFAULT NULL,
  `image` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `category_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `meal_id` int NOT NULL,
  `amount_per_item` decimal(7,2) NOT NULL DEFAULT '0.00',
  `quantity` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `taxes`;
CREATE TABLE `taxes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `percentage` tinyint unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT '0',
  `coupon_id` int NOT NULL DEFAULT '0',
  `tax_id` int NOT NULL DEFAULT '0',
  `status_id` int NOT NULL DEFAULT '0',
  `total_amount` decimal(7,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
('1', 'Burgers', '2020-05-29 14:50:30', '2020-05-29 14:50:30'),
('2', 'Beverages', '2020-05-29 14:50:38', '2020-05-29 14:50:38'),
('3', 'Combo Meals', '2020-05-29 14:50:53', '2020-05-29 14:50:53');

INSERT INTO `coupons` (`id`, `code`, `discount`, `active`, `created_at`, `updated_at`) VALUES
('1', 'GO2018', '10', '1', '2020-05-29 15:05:00', '2020-06-01 02:40:12'),
('2', 'GO2019', '50', '1', '2020-05-31 13:46:28', '2020-06-01 01:38:00'),
('3', 'GO2020', '90', '1', '2020-05-31 13:46:37', '2020-06-01 04:08:45'),
('4', 'GO2021', '70', '1', '2020-05-31 13:46:47', '2020-06-01 02:41:00'),
('5', 'GO2015', '30', '1', '2020-05-31 13:46:57', '2020-06-01 02:17:34');

INSERT INTO `meals` (`id`, `name`, `price`, `image`, `category_id`, `created_at`, `updated_at`) VALUES
('1', 'Hotdog', '69.00', 'https://images.pexels.com/photos/929137/pexels-photo-929137.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', '1', '2020-05-29 14:56:59', '2020-05-29 14:56:59'),
('2', 'Cheese Burger', '79.00', 'https://images.unsplash.com/photo-1565299507177-b0ac66763828?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1264&q=80', '1', '2020-05-29 14:57:19', '2020-05-29 14:57:53'),
('3', 'Fries', '49.00', 'https://images.unsplash.com/photo-1576107232684-1279f390859f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1275&q=80', '1', '2020-05-29 14:58:36', '2020-05-29 14:58:36'),
('4', 'Coke', '45.00', 'https://images.pexels.com/photos/4113669/pexels-photo-4113669.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', '2', '2020-05-29 14:59:16', '2020-05-29 14:59:16'),
('5', 'Sprite', '45.00', 'https://images.unsplash.com/photo-1557746534-7e6ca4397ff5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80', '2', '2020-05-29 14:59:33', '2020-05-29 14:59:33'),
('6', 'Tea', '35.00', 'https://images.unsplash.com/photo-1499638673689-79a0b5115d87?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1300&q=80', '2', '2020-05-29 14:59:49', '2020-05-29 14:59:49'),
('7', 'Chicken Combo', '99.00', 'https://images.pexels.com/photos/2338407/pexels-photo-2338407.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', '3', '2020-05-29 15:01:46', '2020-05-29 15:01:46'),
('8', 'Fish Combo', '109.00', 'https://images.pexels.com/photos/2374946/pexels-photo-2374946.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500', '3', '2020-05-29 15:02:43', '2020-05-29 15:02:43'),
('9', 'Pork Combo', '129.00', 'https://images.unsplash.com/photo-1432139555190-58524dae6a55?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2255&q=80', '3', '2020-05-29 15:02:56', '2020-05-29 15:02:56');

INSERT INTO `products` (`id`, `name`, `created_at`, `updated_at`) VALUES
('1', 'Hotdog', '2020-05-29 14:51:35', '2020-05-29 14:51:35'),
('2', 'Cheese Burger', '2020-05-29 14:51:43', '2020-05-29 14:51:43'),
('3', 'Fries', '2020-05-29 14:51:54', '2020-05-29 14:51:54'),
('4', 'Coke', '2020-05-29 14:52:04', '2020-05-29 14:52:04'),
('5', 'Sprite', '2020-05-29 14:52:11', '2020-05-29 14:52:11'),
('6', 'Tea', '2020-05-29 14:52:15', '2020-05-29 14:52:15'),
('7', 'Fried Chicken', '2020-05-29 14:53:31', '2020-05-29 14:53:31'),
('8', 'Fish Fillet', '2020-05-29 14:53:39', '2020-05-29 14:53:39'),
('9', 'Pork Chop', '2020-05-29 14:53:59', '2020-05-29 14:53:59'),
('10', 'Steamed Rice', '2020-05-29 14:55:03', '2020-05-29 14:55:03');

INSERT INTO `statuses` (`id`, `description`, `created_at`, `updated_at`) VALUES
('1', 'Cancelled', '2020-05-29 15:04:03', '2020-05-29 15:04:03'),
('2', 'Completed', '2020-05-29 15:04:33', '2020-05-29 15:04:33');

INSERT INTO `taxes` (`id`, `percentage`, `active`, `created_at`, `updated_at`) VALUES
('1', '12', '1', '2020-05-29 15:04:51', '2020-05-29 15:04:51');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;