<?php

namespace App\Repositories;

use App\Order;

/**
 * Class OrderRepository.
 */
class OrderRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return new Order;
    }

    public function storeOrders($orders)
    {
        $orderIDs = [];
        foreach ($orders as $order) {
            $data = $this->model();
            $data->meal_id = $order['id'];
            $data->amount_per_item = $order['price'];
            $data->quantity = $order['quantity'];
            $data->save();
            $orderIDs[] = $data->id;
        }
        return $orderIDs;
    }
}
