<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['user_id', 'coupon_id', 'tax_id', 'status_id', 'total_amount'];

    protected $appends = ['status_description'];

    protected $with = ['coupon', 'tax', 'status', 'orders'];

    protected $hidden = ['status'];

    // Attributes
    public function getStatusDescriptionAttribute()
    {
        return $this->status->description;
    }

    // Relationships
    public function tax()
    {
        return $this->hasOne('App\Tax', 'id', 'tax_id');
    }

    public function coupon()
    {
        return $this->hasOne('App\Coupon', 'id', 'coupon_id');
    }

    public function status()
    {
        return $this->hasOne('App\Status', 'id', 'status_id');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }
}
