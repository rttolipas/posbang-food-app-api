<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    protected $with = ['meals'];

    public function meals()
    {
        return $this->hasMany('App\Meal', 'category_id', 'id');
    }
}
