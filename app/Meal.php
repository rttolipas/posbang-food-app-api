<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    protected $fillable = ['name', 'price', 'image', 'category_id'];

    protected $with = ['products'];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
