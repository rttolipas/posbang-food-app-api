<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['amount_per_item', 'meal_id', 'quantity'];

    protected $appends = ['name', 'image', 'products', 'price'];

    protected $hidden = ['meal', 'amount_per_item'];

    // Attributes
    public function getNameAttribute()
    {
        return $this->meal->name;
    }

    public function getImageAttribute()
    {
        return $this->meal->image;
    }

    public function getProductsAttribute()
    {
        return $this->meal->products;
    }

    public function getPriceAttribute()
    {
        return $this->meal->price;
    }

    // Relationship
    public function meal()
    {
        return $this->hasOne('App\Meal', 'id', 'meal_id');
    }
}
