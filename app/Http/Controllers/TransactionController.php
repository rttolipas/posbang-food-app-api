<?php

namespace App\Http\Controllers;

use App\Repositories\OrderRepository;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends ApiController
{
    public function __construct()
    {
        $this->orderRepository = new OrderRepository;
    }

    public function index()
    {
        $data = Transaction::query()
                            ->orderBy('created_at', 'desc')
                            ->get();
        return $this->sendResponse($data, 'Successfully show all data');
    }

    public function store(Request $request)
    {
        $data = new Transaction;
        $data->user_id = Auth::id();
        $data->coupon_id = $request->coupon_id;
        $data->tax_id = $request->tax_id;
        $data->status_id = $request->status_id;
        $data->total_amount = $request->total_amount;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            $orderIds = $this->orderRepository->storeOrders($request->_orders);
            $data->orders()->sync($orderIds);
            return $this->sendResponse($data, 'Successfully store data');
        }
    }

    public function show(Transaction $transaction, $id)
    {
        $data = $transaction::findOrFail($id);
        return $this->sendResponse($data, 'Successfully show data');
    }

    public function update(Request $request, Transaction $transaction)
    {
        $data = $transaction::findOrFail($request->id);
        $data->user_id = Auth::id();
        $data->coupon_id = $request->coupon_id;
        $data->tax_id = $request->tax_id;
        $data->status_id = $request->status_id;
        $data->total_amount = $request->total_amount;

        $data->orders()->sync($request->orders);

        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully update data');
        }
    }

    public function destroy(Transaction $transaction, $id)
    {
        $data = $transaction::findOrFail($id);
        $data->delete();
        return $this->sendResponse($data, 'Successfully destroy data');
    }
}
