<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends ApiController
{
    public function index()
    {
        $data = Order::all();
        return $this->sendResponse($data, 'Successfully show all data');
    }

    public function store(Request $request)
    {
        $data = new Order;
        $data->meal_id = $request->meal_id;
        $data->amount_per_item = $request->amount_per_item;
        $data->quantity = $request->quantity;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully store data');
        }
    }

    public function show(Order $order, $id)
    {
        $data = $order::findOrFail($id);
        return $this->sendResponse($data, 'Successfully show data');
    }

    public function update(Request $request, Order $order)
    {
        $data = $order::findOrFail($request->id);
        $data->meal_id = $request->meal_id;
        $data->amount_per_item = $request->amount_per_item;
        $data->quantity = $request->quantity;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully update data');
        }
    }

    public function destroy(Order $order, $id)
    {
        $data = $order::findOrFail($id);
        $data->delete();
        return $this->sendResponse($data, 'Successfully destroy data');
    }
}
