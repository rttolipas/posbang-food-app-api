<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends ApiController
{
    public function index()
    {
        $data = Product::all();
        return $this->sendResponse($data, 'Successfully show all data');
    }

    public function store(Request $request)
    {
        $data = new Product;
        $data->name = $request->name;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully store data');
        }
    }

    public function show(Product $product, $id)
    {
        $data = $product::findOrFail($id);
        return $this->sendResponse($data, 'Successfully show data');
    }

    public function update(Request $request, Product $product)
    {
        $data = $product::findOrFail($request->id);
        $data->name = $request->name;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully update data');
        }
    }

    public function destroy(Product $product, $id)
    {
        $data = $product::findOrFail($id);
        $data->delete();
        return $this->sendResponse($data, 'Successfully destroy data');
    }
}
