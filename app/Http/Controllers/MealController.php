<?php

namespace App\Http\Controllers;

use App\Meal;
use Illuminate\Http\Request;

class MealController extends ApiController
{
    public function index()
    {
        $data = Meal::all();
        return $this->sendResponse($data, 'Successfully show all data');
    }

    public function store(Request $request)
    {
        $data = new Meal;
        $data->name = $request->name;
        $data->price = $request->price;
        $data->image = $request->image;
        $data->category_id = $request->category_id;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            $data->products()->sync($request->products);
            return $this->sendResponse($data, 'Successfully store data');
        }
    }

    public function show(Meal $meal, $id)
    {
        $data = $meal::findOrFail($id);
        return $this->sendResponse($data, 'Successfully show data');
    }

    public function update(Request $request, Meal $meal)
    {
        $data = $meal::findOrFail($request->id);
        $data->name = $request->name;
        $data->price = $request->price;
        $data->image = $request->image;
        $data->category_id = $request->category_id;
        $data->products()->sync($request->products);
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully update data');
        }
    }

    public function destroy(Meal $meal, $id)
    {
        $data = $meal::findOrFail($id);
        $data->delete();
        return $this->sendResponse($data, 'Successfully destroy data');
    }
}
