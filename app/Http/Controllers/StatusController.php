<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;

class StatusController extends ApiController
{
    public function index()
    {
        $data = Status::all();
        return $this->sendResponse($data, 'Successfully show all data');
    }

    public function store(Request $request)
    {
        $data = new Status;
        $data->description = $request->description;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully store data');
        }
    }

    public function show(Status $status, $id)
    {
        $data = $status::findOrFail($id);
        return $this->sendResponse($data, 'Successfully show data');
    }

    public function update(Request $request, Status $status)
    {
        $data = $status::findOrFail($request->id);
        $data->description = $request->description;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully update data');
        }
    }

    public function destroy(Status $status, $id)
    {
        $data = $status::findOrFail($id);
        $data->delete();
        return $this->sendResponse($data, 'Successfully destroy data');
    }
}
