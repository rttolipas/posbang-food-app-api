<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends ApiController
{
    public function index()
    {
        $data = Coupon::all();
        return $this->sendResponse($data, 'Successfully show all data');
    }

    public function store(Request $request)
    {
        $data = new Coupon;
        $data->code = $request->code;
        $data->discount = $request->discount;
        $data->active = $request->active;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully store data');
        }
    }

    public function show(Coupon $coupon, $id)
    {
        $data = $coupon::findOrFail($id);
        return $this->sendResponse($data, 'Successfully show data');
    }

    public function update(Request $request, Coupon $coupon)
    {
        $data = $coupon::findOrFail($request->id);
        $data->code = $request->code;
        $data->discount = $request->discount;
        $data->active = $request->active;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully update data');
        }
    }

    public function destroy(Coupon $coupon, $id)
    {
        $data = $coupon::findOrFail($id);
        $data->delete();
        return $this->sendResponse($data, 'Successfully destroy data');
    }

    public function apply(Request $request, Coupon $coupon)
    {
        if (! $request->code) {
            return $this->sendError('No code found');
        }
        $data = $coupon::where('code', strtolower($request->code))
                        ->where('active', true)
                        ->first();

        if (! $data) {
            return $this->sendError('Invalid coupon code');
        } else {
            $data->active = false;
            $data->save();
            return $this->sendResponse($data, 'Successfully check data');
        }
    }
}
