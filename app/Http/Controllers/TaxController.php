<?php

namespace App\Http\Controllers;

use App\Tax;
use Illuminate\Http\Request;

class TaxController extends ApiController
{
    public function index()
    {
        $data = Tax::all();
        return $this->sendResponse($data, 'Successfully show all data');
    }

    public function store(Request $request)
    {
        $data = new Tax;
        $data->percentage = $request->percentage;
        $data->active = $request->active;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully store data');
        }
    }

    public function show(Tax $tax, $id)
    {
        $data = $tax::findOrFail($id);
        return $this->sendResponse($data, 'Successfully show data');
    }

    public function update(Request $request, Tax $tax)
    {
        $data = $tax::findOrFail($request->id);
        $data->percentage = $request->percentage;
        $data->active = $request->active;
        if (! $data->save()) {
            return $this->sendError('Error saving data.');
        } else {
            return $this->sendResponse($data, 'Successfully update data');
        }
    }

    public function destroy(Tax $tax, $id)
    {
        $data = $tax::findOrFail($id);
        $data->delete();
        return $this->sendResponse($data, 'Successfully destroy data');
    }

    public function active(Tax $tax)
    {
        $data = $tax::where('active', true)->first();
        return $this->sendResponse($data, 'Successfully fetch data');
    }
}
