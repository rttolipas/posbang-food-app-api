<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Auth
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});

// Products
Route::group([
    'prefix' => 'products',
    'middleware' => 'auth:api'
], function () {
    Route::get('/', 'ProductController@index');
    Route::post('/', 'ProductController@store');
    Route::get('/{id}', 'ProductController@show');
    Route::put('/{id}', 'ProductController@update');
    Route::delete('/{id}', 'ProductController@destroy');
});

// Categories
Route::group([
    'prefix' => 'categories',
], function () {
    Route::get('/', 'CategoryController@index');
    Route::post('/', 'CategoryController@store')->middleware(['auth:api']);
    Route::get('/{id}', 'CategoryController@show');
    Route::put('/{id}', 'CategoryController@update')->middleware(['auth:api']);
    Route::delete('/{id}', 'CategoryController@destroy')->middleware(['auth:api']);
});

// Meals
Route::group([
    'prefix' => 'meals',
], function () {
    Route::get('/', 'MealController@index');
    Route::post('/', 'MealController@store')->middleware(['auth:api']);
    Route::get('/{id}', 'MealController@show');
    Route::put('/{id}', 'MealController@update')->middleware(['auth:api']);
    Route::delete('/{id}', 'MealController@destroy')->middleware(['auth:api']);
});

// Statuses
Route::group([
    'prefix' => 'statuses',
    'middleware' => 'auth:api'
], function () {
    Route::get('/', 'StatusController@index');
    Route::post('/', 'StatusController@store');
    Route::get('/{id}', 'StatusController@show');
    Route::put('/{id}', 'StatusController@update');
    Route::delete('/{id}', 'StatusController@destroy');
});

// Coupons
Route::group([
    'prefix' => 'coupons',
    'middleware' => 'auth:api'
], function () {
    Route::get('/', 'CouponController@index');
    Route::post('/', 'CouponController@store');
    Route::get('/{id}', 'CouponController@show');
    Route::put('/{id}', 'CouponController@update');
    Route::delete('/{id}', 'CouponController@destroy');

    Route::post('/validate', 'CouponController@apply');
});

// Taxes
Route::group([
    'prefix' => 'taxes',
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('/', 'TaxController@index');
        Route::post('/', 'TaxController@store');
        Route::get('/{id}', 'TaxController@show');
        Route::put('/{id}', 'TaxController@update');
        Route::delete('/{id}', 'TaxController@destroy');
    });

    Route::post('/active', 'TaxController@active');
});

// Orders
Route::group([
    'prefix' => 'orders',
    'middleware' => 'auth:api'
], function () {
    Route::get('/', 'OrderController@index');
    Route::post('/', 'OrderController@store');
    Route::get('/{id}', 'OrderController@show');
    Route::put('/{id}', 'OrderController@update');
    Route::delete('/{id}', 'OrderController@destroy');
});

// Transaction
Route::group([
    'prefix' => 'transactions',
    'middleware' => 'auth:api'
], function () {
    Route::get('/', 'TransactionController@index');
    Route::post('/', 'TransactionController@store');
    Route::get('/{id}', 'TransactionController@show');
    Route::put('/{id}', 'TransactionController@update');
    Route::delete('/{id}', 'TransactionController@destroy');
});
